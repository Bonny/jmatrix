package modello;

/**
 * pattern per identificare i valori che possono assumere
 * le entrate di una generica matrice M
 * @author bonny
 */
public class Generici {

    /**
     *
     * pattern idendificatore numeri interi
     *
     */
    public static final String INT = "([\\-]?[0-9]+)";
    /**
     *
     * pattern idendificatore numeri con virgola mobile
     *
     */
    public static final String FLOAT = "([\\-]?[0-9]+\\.([0-9]+))";
    /**
     *
     * pattern idendificatore numeri irrazionali
     *
     */
    public static final String INR = "([\\-]?[0-9]+[\\/]([1-9]+[0-9]*+))";
    /**
     *
     * pattern identificatore della matrice
     *
     */
    public static final String ID = "[a-zA-z]{1}[a-zA-z0-9]*";
    /**
     *
     * pattern idendificatore entrate della matrice
     *
     */
    public static final String VALUE = INT + "|" + FLOAT + "|" + INR;
    /**
     *
     * pattern idendificatore dalle righe della matrice
     *
     */
    public static final String ROW = "([\\(]((" + VALUE + ")([\\,](" + VALUE + "))*)[\\)])+";
    /**
     *
     * pattern idendificatore dalla matrice
     *
     */
    public static final String MAT = "(\\[)" + ROW + "(\\])";
    /**
     *
     * pattern idendificatore serie di matrici lette dal file
     *
     */
    public static final String MATRICI = "(" + Generici.ID + "[\\=]" + Generici.MAT + ")"
            + "([\\;]" + Generici.ID + "[\\=]" + Generici.MAT + ")*";
}
