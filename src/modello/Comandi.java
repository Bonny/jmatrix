package modello;

/**
 * pattern dei comandi relativi alle istruzioni algebriche
 * operanti sulle matrici
 * @author bonny
 */
public class Comandi {

    /**
     *
     * pattern risolvimento sistema lineare
     * associato ad una generica matrice M
     *
     */
    public static final String SISTEMA = "[S][Y][S][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo della matrice inversa
     * di una generica matrice M
     *
     */
    public static final String INVERSA = "[I][N][V][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo della matrice trasposta
     * di una generica matrice M
     *
     */
    public static final String TRASPOSTA = "[T][R][S][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo del rango
     * di una generica matrice M
     *
     */
    public static final String RANGO = "[R][G][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo del determinante
     * di una generica matrice M
     *
     */
    public static final String DETERMINANTE = "[D][E][T][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo della traccia
     * di una generica matrice M
     *
     */
    public static final String TRACCIA = "[T][R][\\(]" + Generici.ID + "[\\)]";
    /**
     *
     * pattern calcolo della somma di
     * due o più matrici
     *
     */
    public static final String SUMM = Generici.ID + "([//+]" + Generici.ID + ")+";
    /**
     *
     * pattern calcolo della differenza di
     * due o più matrici
     *
     */
    public static final String DIFF = Generici.ID + "([//-]" + Generici.ID + ")+";
    /**
     *
     * pattern calcolo del prodotto
     * di due o più matrici
     *
     */
    public static final String PROD_MAT = Generici.ID + "([//*]" + Generici.ID + ")+";
    /**
     *
     * pattern calcolo del prodotto
     * tra uno scalare e una generica matrice
     *
     */
    public static final String PROD_SCAL = "(" + Generici.VALUE + ")[\\*]" + Generici.ID;
    /**
     *
     * pattern calcolo del polinomio caratteristico
     *
     *
     */
    public static final String POLINOMIO = "[P][\\(]" + Generici.ID + "[\\)]";
       /**
     *
     * pattern calcolo auto valori matrice
     *
     *
     */
    public static final String AUTOVALORI = "[A][V][L][\\(]" + Generici.ID + "[\\)]";

}
