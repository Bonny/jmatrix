package modello;

/**
 * 
 * @author bonny
 */
public class Dichiarazioni {

    /**
     *
     * pattern relativo alla definizione di una nuova matrice:
     * base(pattern MAT) o mediante altre funzioni di calcolo (pattern INVERSA,TRASPOSTA ecc..)
     *
     */
    public static final String NEW_MAT = "(" + Generici.ID + "[\\=]" + Generici.MAT + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.INVERSA + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.TRASPOSTA + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.PROD_MAT + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.PROD_SCAL + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.DIFF + ")"
            + "|(" + Generici.ID + "[\\=]" + Comandi.SUMM + ")";
}
