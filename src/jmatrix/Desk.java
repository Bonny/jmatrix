package jmatrix;

import algebra.Algebra;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.html.HTMLEditorKit;

/**
 * 
 * @author bonny
 */
public class Desk extends JPanel {

    private SubDesk edit;
    private ArrayList<Elem> listaElem;
    private final String css = "<style>"
            + "body{font-size:11px;background-color:white;}"
            + "small{font-size:10px;}"
            + "table.esterna{background-color:white;}"
            + "table.interna{background-color:white;}"
            + "div.matrice{padding:0px 2px 0px 2px;background-color:#000;}"
            + "</style><!--Algebra-->";

    /**
     * 
     * @param pane
     */
    public Desk(final JTabbedPane pane) {
        listaElem = new ArrayList<Elem>();
        edit = new SubDesk(pane);
        edit.setText(css);
        this.init();
    }

    /**
     * 
     * @param page
     * @param lista
     * @param pane
     */
    public Desk(String page, ArrayList<Elem> lista, final JTabbedPane pane) {
        edit = new SubDesk(pane);
        edit.setText(page);
        this.listaElem = lista;
        this.init();
    }

    /**
     * 
     * @param e
     */
    public void addElem(Elem e) {
        this.listaElem.add(e);
    }

    /**
     * 
     * @return
     */
    public String getProgetto() {
        return this.edit.getText();
    }

    /**
     * 
     * @return
     */
    public ArrayList<Elem> getListElem() {
        return this.listaElem;
    }

    /**
     * 
     * @param msg
     */
    public void add(String msg) {
        String v[] = edit.getText().split("\\<\\/body\\>");
        this.edit.setText(v[0] + "<br>" + msg + "</body>" + v[1]);
    }

    /**
     * 
     * @param str
     * @param mat
     * @param numCifre
     */
    public void add(String str, double mat[][], int numCifre) {

        String html = "<table class='esterna' cellpadding='10' cellspacing='0'>";
        html += "<tr>";
        html += "<td>" + str + "</td>";
        html += "<td><div class='matrice'><table class='interna' cellpadding='10'>";
        for (int j = 0; j < mat.length; j++) {
            html += "<tr>";
            for (int k = 0; k < mat[0].length; k++) {
                html += "<td>" + Algebra.tronca(numCifre, mat[j][k]) + "</td>";
            }
            html += "</tr>";
        }
        html += "</table></div></td>";
        html += "</tr></table>";
        String v[] = edit.getText().split("\\<\\/body\\>");

        this.edit.setText(v[0] + "<br>" + html + "</body>" + v[1]);
        //System.out.println(edit.getText());

        //this.isStato = false;
    }

    /**
     * 
     * @param nome
     * @param list
     * @param op
     * @param numCifre
     */
    public void add(String nome, ArrayList<double[][]> list, String op, int numCifre) {
        String html = "<table class='esterna' cellpadding='10' cellspacing='0'>";
        html += "<tr>";
        html += "<td>" + nome + "</td>";
        for (int i = 0; i < list.size(); i++) {

            html += "<td><div class='matrice'><table class='interna' cellpadding='10'>";
            double mat[][] = list.get(i);

            for (int j = 0; j < mat.length; j++) {
                html += "<tr>";
                for (int k = 0; k < mat[0].length; k++) {
                    html += "<td>" + Algebra.tronca(numCifre, mat[j][k]) + "</td>";
                }
                html += "</tr>";
            }

            html += "</table></div></td>";
            if (i < list.size() - 2) {
                html += "<td>" + op + "</td>";
            } else if (i == list.size() - 2) {
                html += "<td>=</td>";
            }
        }
        html += "</tr></table>";
        String v[] = edit.getText().split("\\<\\/body\\>");
        this.edit.setText(v[0] + "<br>" + html + "</body>" + v[1]);

        //this.isStato = false;
    }

    /**
     * 
     * @param id
     * @return
     */
    public double[][] getMatrice(String id) {
        double m[][] = null;
        boolean flag = false;
        int i = 0;
        while (i < this.listaElem.size() && !flag) {
            if (this.listaElem.get(i).compara(id)) {
                m = listaElem.get(i).getMatrice();
                flag = true;
            } else {
                i++;
            }
        }
        return m;
    }

    /**
     * 
     * @param o
     * @return
     */
    public boolean esiste(String o) {
        boolean flag = false;
        int i = 0;
        while (i < this.listaElem.size() && !flag) {
            if (this.listaElem.get(i).compara(o)) {
                flag = true;
            } else {
                i++;
            }
        }
        return flag;
    }

    private void init() {

        this.setBackground(Color.white);
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints lim = new GridBagConstraints();
        this.setLayout(layout);
        lim.gridx = 0;
        lim.gridy = 0;
        lim.weightx = 1;
        lim.weighty = 1;
        lim.anchor = GridBagConstraints.NORTHWEST;
        lim.fill = GridBagConstraints.BOTH;
        layout.setConstraints(edit, lim);
        this.add(edit);

    }

    private class SubDesk extends JEditorPane {

        public SubDesk(final JTabbedPane pane) {
            super();
            this.setEditable(false);
            this.setEditorKit(new HTMLEditorKit());
            this.setBackground(Color.white);
            this.addCaretListener(new CaretListener() {

                @Override
                public void caretUpdate(CaretEvent ce) {
                    if (pane.getSelectedIndex() != -1 && ce.getDot() == 0 && ce.getMark() == 0) {
                        ButtonTab x = (ButtonTab) pane.getTabComponentAt(pane.getSelectedIndex());
                        x.setStato(false);
                    }
                }
            });
        }
    }
}

