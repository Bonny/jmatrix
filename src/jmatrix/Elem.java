package jmatrix;

/**
 *
 * @author bonny
 */
public class Elem {

    private double matrice[][];
    private String nome;

    /**
     * 
     * @param n
     * @param m
     */
    public Elem(String n, double m[][]) {
        this.nome = n;
        this.matrice = m;
    }

    /**
     * 
     * @return
     */
    public double[][] getMatrice() {
        return this.matrice;
    }

    /**
     * 
     * @return
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * 
     * @param o
     * @return
     */
    public boolean compara(String o) {
        return (this.nome.equals(o));
    }
}
