package jmatrix;

import algebra.Algebra;
import algebra.Complesso;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultEditorKit;
import modello.Comandi;
import modello.Dichiarazioni;
import modello.Generici;

/**
 *
 * @author bonny
 */
public class MyFrame extends JFrame implements ActionListener, ItemListener {

  private int count = 0;
  private final String tab = " &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ";
  private int numCifre;
  private JButton btnOk;
  private JTextField txtExpr;
  private JTabbedPane tabPane;
  private Finestra finestra;
  private FinestraInfo info = new FinestraInfo();
  /**
   *
   */
  public MyFrame() {
    super("JMatrix");
    finestra = new Finestra();
    this.setJMenuBar(creaMenuBar());
    this.initUI();
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    this.setLocation(0, 0);
    this.setSize((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(), (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight());
    txtExpr.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == 10) {
          ScrollDesk sc = (ScrollDesk) tabPane.getSelectedComponent();
          scanner(sc.getDesk());
        }
      }
    });
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        close();
      }
    });

    addDesk();
    this.setBounds(600, 0, 800, 500);
  }

  @Override
  public void actionPerformed(ActionEvent ae) {
    String e = ae.getActionCommand();
    switch (e) {
      case "Esci":
        close();
        break;
      case "Nuovo progetto":
        addDesk();
        break;
      case "Apri progetto":
        ApriSalvaProgetto.apri(tabPane);
        break;
      case "Salva": {
        final int index = tabPane.getSelectedIndex();
        if (index != -1) {
          ButtonTab bt = (ButtonTab) tabPane.getTabComponentAt(index);
          if (bt.getFile() != null) {
            ScrollDesk sc = (ScrollDesk) tabPane.getSelectedComponent();
            ApriSalvaProgetto.salva(sc.getDesk(), bt.getFile());
            bt.setStato(true);
          } else {
            if (!bt.getStato()) {
              JFileChooser chooser = new JFileChooser();
              chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));
              chooser.setMultiSelectionEnabled(false);
              if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = new File(chooser.getSelectedFile().getAbsolutePath() + ".alg");
                ScrollDesk sc = (ScrollDesk) tabPane.getSelectedComponent();
                ApriSalvaProgetto.salva(sc.getDesk(), file);
                bt.setFile(file);
                bt.setText(file.getName());
                bt.setStato(true);
              } else {
                break;
              }
            }
          }
        }
        break;
      }
      case "Salva con nome": {
        final int index = tabPane.getSelectedIndex();
        if (index != -1) {
          ButtonTab bt = (ButtonTab) tabPane.getTabComponentAt(index);
          if (bt.getFile() == null) {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));
            chooser.setMultiSelectionEnabled(false);
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
              File file = new File(chooser.getSelectedFile().getAbsolutePath() + ".alg");
              ScrollDesk sc = (ScrollDesk) tabPane.getSelectedComponent();
              ApriSalvaProgetto.salva(sc.getDesk(), file);
              bt.setFile(file);
              bt.setText(file.getName());
              bt.setStato(true);
            } else {
              break;
            }
          }
        }
        break;
      }
      case "Info":
        info.setVisible(true);
        break;
      case "Vai":
        ScrollDesk sc = (ScrollDesk) tabPane.getSelectedComponent();
        scanner(sc.getDesk());
        break;
    }
  }

  private void addDesk() {
    Desk desk = new Desk(tabPane);
    tabPane.addTab((++count) + "~Algebra", new ScrollDesk(desk));
    int c = tabPane.getTabCount() - 1;
    ButtonTab bt = new ButtonTab(tabPane);
    bt.setStato(false);
    tabPane.setTabComponentAt(c, bt);
    tabPane.setSelectedIndex(c);

  }

  private void close() {

    if (tabPane.getTabCount() > 0) {
      finestra.init(tabPane);
      finestra.setVisible(true);
    } else {
      System.exit(0);
    }

  }

  private void reset() {
    txtExpr.setText("");
    txtExpr.requestFocus();
  }

  private void scanner(Desk desk) {

    String str = txtExpr.getText().replaceAll(" ", "");

    if (str.matches(Dichiarazioni.NEW_MAT)) {

      String v[] = str.split("\\=", 2);

      if (!desk.esiste(v[0])) {

        if (v[1].matches(Generici.MAT)) {
          double[][] m = parseMatrice(v[1]);
          if (m != null) {
            Elem e = new Elem(v[0], m);
            desk.addElem(e);
            desk.add("<b>" + v[0] + "</b> = ", m, numCifre);
            reset();
          } else {
            desk.add("<font color='#BE1110'>" + tab + "> Errore di sintassi</font>");
          }
        } else if (v[1].matches(Comandi.INVERSA)) {
          v[1] = v[1].replaceFirst("INV\\(", "");
          v[1] = v[1].replaceFirst("\\)", "");
          if (desk.esiste(v[1])) {
            double[][] m = desk.getMatrice(v[1]);
            if (m.length == m[0].length) {
              m = Algebra.inversa(m);
              if (m != null) {
                Elem e = new Elem(v[0], m);
                desk.addElem(e);
                desk.add("<b>" + v[0] + "</b> = <b>" + v[1] + "<small><sup>-1</sup></small></b> = ", m, numCifre);
                reset();
              } else {
                desk.add("<font color='#416E32'>" + tab + "> <b>" + v[1] + "</b> non è invertibile</font>");
              }
            } else {
              desk.add("<font color='#416E32'>" + tab + "> <b>" + v[1] + "</b> non è una matrice quadrata impossibile"
                      + " calcolare <b>" + v[1] + "<small><sup>-1</sup></small></b></font>");
            }
          } else {
            desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + v[1] + "</b> non esiste</font>");
          }
        } else if (v[1].matches(Comandi.TRASPOSTA)) {
          v[1] = v[1].replaceFirst("TRS\\(", "");
          v[1] = v[1].replaceFirst("\\)", "");
          if (desk.esiste(v[1])) {
            double[][] m = Algebra.trasposta(desk.getMatrice(v[1]));
            if (m != null) {
              Elem e = new Elem(v[0], m);
              desk.addElem(e);
              desk.add("<b>" + v[0] + "</b> = <b>" + v[1] + "<small><sub>t</sub></small></b> = ", m, numCifre);
              reset();
            }
          } else {
            desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + v[1] + "</b> non esiste</font>");
          }
        } else if (v[1].matches(Comandi.PROD_MAT)) {
          parseAndCalcola(desk, v[0], v[1], "*");
        } else if (v[1].matches(Comandi.SUMM)) {
          parseAndCalcola(desk, v[0], v[1], "+");
        } else if (v[1].matches(Comandi.DIFF)) {
          parseAndCalcola(desk, v[0], v[1], "-");
        } else if (v[1].matches(Comandi.PROD_SCAL)) {
          String c[] = v[1].split("\\*");
          if (desk.esiste(c[1])) {
            double m[][] = Algebra.prodotto(parseValue(c[0]), desk.getMatrice(c[1]));
            Elem e = new Elem(v[0], m);
            desk.addElem(e);
            desk.add("<b>" + v[0] + "</b> = " + c[0] + " * " + c[1] + " = ", m, numCifre);
          } else {
            desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
          }
        }
      } else {
        desk.add("<font color='#163279'>" + tab + "> Esiste già una matrice con  ID = <b>" + v[0] + "</b></font>");
      }
    } else if (str.matches(Comandi.DETERMINANTE)) {
      str = str.replaceFirst("DET\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        double m[][] = desk.getMatrice(str);
        if (m != null) {
          if (m.length == m[0].length) {
            desk.add(tab + "<b><i>det</i>(" + str + ") = " + Algebra.tronca(numCifre, Algebra.det(m)) + "</b>");
            reset();
          } else {
            desk.add("<font color='#416E32'>" + tab + "> <b>" + str + "</b> non è una matrice quadrata "
                    + "impossibile calcolare <b><i>det</i>(" + str + ")</b></font>");
          }
        }
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.DIFF)) {
      parseAndCalcola(desk, "", str, "-");
    } else if (str.matches(Comandi.INVERSA)) {
      str = str.replaceFirst("INV\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        double[][] m = desk.getMatrice(str);
        if (m.length == m[0].length) {
          m = Algebra.inversa(m);
          if (m != null) {
            desk.add("<b>" + str + "<small><sup>-1</sup></small></b> = ", m, numCifre);
            reset();
          } else {
            desk.add("<font color='#416E32'>" + tab + "> <b>" + str + "</b> non è invertibile</font>");
          }
        } else {
          desk.add("<font color='#416E32'>" + tab + "> <b>" + str + "</b> non è una matrice quadrata "
                  + "impossibile calcolare <b>" + str + "<small><sup>-1</sup></small></b></font>");
        }
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.PROD_MAT)) {
      parseAndCalcola(desk, "", str, "*");
    } else if (str.matches(Comandi.PROD_SCAL)) {
      String c[] = str.split("\\*");
      if (desk.esiste(c[1])) {
        desk.add(c[0] + " * " + c[1] + " = ", Algebra.prodotto(parseValue(c[0]), desk.getMatrice(c[1])), numCifre);
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.RANGO)) {
      str = str.replaceFirst("RG\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        desk.add("<b>" + tab + "<i>rg</i>(" + str + ") = " + Algebra.rango(desk.getMatrice(str)) + "</b>");
        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.SISTEMA)) {

      str = str.replaceFirst("SYS\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        double m[][] = desk.getMatrice(str);
        int n = Algebra.sysRCapelli(m);
        if (n == 0) {
          double s[] = Algebra.sysCramer(m);
          if (s != null) {
            String tmp = "<b>" + tab + "Unica soluzione:</b><br><br>" + tab;
            for (int i = 0; i < s.length; i++) {
              tmp += "<b>x<small><sub>" + i + "</sub></small></b> = "
                      + Algebra.tronca(numCifre, s[i]) + ";" + tab;
            }
            desk.add(tmp);
            reset();
          }
        } else if (n > 0) {
          desk.add("<b>" + tab + "Soluzioni possiboli:</b><strong>" + tab + " &infin </strong><small><sup>" + n + "</sup></small>");
          reset();
        } else if (n < 0) {
          desk.add("<font color='#416E32'>" + tab + "> Impossibile trovare soluzioni del sistema</font>");
        }
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.SUMM)) {
      parseAndCalcola(desk, "", str, "+");
    } else if (str.matches(Comandi.TRACCIA)) {
      str = str.replaceFirst("TR\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        desk.add("<b>" + tab + "<i>tr</i>(" + str + ") = "
                + Algebra.tronca(numCifre, Algebra.traccia(desk.getMatrice(str))) + "</b>");
        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.TRASPOSTA)) {
      str = str.replaceFirst("TRS\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        desk.add("<b>" + str + "<small><sub>t</sub></small></b> = ", Algebra.trasposta(desk.getMatrice(str)), numCifre);
        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.POLINOMIO)) {
      str = str.replaceFirst("P\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        double coef[] = Algebra.polinomio(desk.getMatrice(str));
        if (coef != null) {
          desk.add(creaHTMLPolinomio(str, coef));
        } else {
          desk.add("<font color='#163279'>" + tab + "> Impossibile calcolare il polinomio caratteristico di <b>" + str + "</b></font>");
        }
        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else if (str.matches(Comandi.AUTOVALORI)) {
      str = str.replaceFirst("AVL\\(", "");
      str = str.replaceFirst("\\)", "");
      if (desk.esiste(str)) {
        double polinomio[] = Algebra.polinomio(desk.getMatrice(str));

//                for (int i = polinomio.length - 1; i >= 0; i--) {
//                    System.out.println(polinomio[i]);
//                }
        if (polinomio != null) {
          desk.add(creaHTMLPolinomio(str, polinomio));
          if (polinomio.length == 3) {
            double autoValori[] = Algebra.risolvi_g2(polinomio);
            if (autoValori != null) {
              String html = "<b> " + tab;
              for (int i = 0; i < autoValori.length; i++) {
                html += "  &lambda;<sub><small>" + i + "</small></sub><small>=</small> "
                        + Algebra.tronca(numCifre, autoValori[i]) + "; &nbsp&nbsp&nbsp ";
              }
              html += "</b>";
              desk.add("<br><br>" + html);
            } else {
              desk.add("<font color='#163279'>" + tab + "> Nessuna soluzione del polinomio caratteristico P(" + str + ")</font>");
            }
          } else if (polinomio.length == 4) {
            Complesso autoValori[] = Algebra.risolvi_g3(polinomio);
            if (autoValori != null) {
              String html = "<b> " + tab;
              for (int i = 0; i < autoValori.length; i++) {
                html += "  &lambda;<sub><small>" + i + "</small></sub><small>=</small> "
                        + Algebra.tronca(numCifre, autoValori[i].getRe()) + "&nbsp&nbsp"
                        + Algebra.tronca(numCifre, autoValori[i].getIm())
                        + "; &nbsp&nbsp&nbsp ";
              }
              html += "</b>";
              desk.add("<br><br>" + html);
            } else {
              desk.add("<font color='#163279'>" + tab + "> Nessuna soluzione del polinomio caratteristico P(" + str + ")</font>");
            }
          }
        } else {
          desk.add("<font color='#163279'>" + tab + "> Impossibile calcolare il polinomio caratteristico di <b>" + str + "</b></font>");
        }

        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> La matrice <b>" + str + "</b> non esiste</font>");
      }
    } else {
      if (!str.equals("")) {
        desk.add("<font color='#BE1110'>" + tab + "> Errore di sintassi</font>");
      }
    }
  }

  /**
   *
   * @param str
   * @return
   */
  public static double[][] parseMatrice(String str) {

    double M[][] = null;
    str = str.replaceFirst("\\[\\(", "");
    str = str.replaceFirst("\\)\\]", "");
    String row[] = str.split("\\)\\(");
    ArrayList<String[]> list = new ArrayList<String[]>();
    for (String o : row) {
      list.add(o.split("\\,"));
    }
    final int col = list.get(0).length;
    boolean flag = false;
    int i = 1;
    while (i < list.size() && !flag) {
      if (list.get(i).length != col) {
        flag = true;
      } else {
        i++;
      }
    }
    if (!flag) {
      M = new double[list.size()][col];
      int countRow = 0, countCol;
      for (String riga[] : list) {
        countCol = 0;
        for (String cella : riga) {
          M[countRow][countCol] = parseValue(cella);
          countCol++;
        }
        countRow++;
      }
    }
    return M;
  }

  private void parseAndCalcola(Desk desk, String new_id, String s, String op) {

    String id[] = s.split("\\" + op);
    int i = 0;
    boolean flag = false;
    while (i < id.length && !flag) {
      if (!desk.esiste(id[i])) {
        flag = true;
      } else {
        i++;
      }
    }
    if (!flag) {
      ArrayList<double[][]> matrici = new ArrayList<double[][]>();
      for (String o : id) {
        matrici.add(desk.getMatrice(o));
      }
      final int nRow = matrici.get(0).length;
      final int nCol = matrici.get(0)[0].length;
      i = 1;
      flag = false;
      while (i < matrici.size() && !flag) {
        if (matrici.get(i).length != nRow && matrici.get(i)[0].length != nCol) {
          flag = true;
        } else {
          i++;
        }
      }
      if (!flag) {
        double r[][] = (double[][]) matrici.get(0).clone();
        for (i = 1; i < matrici.size(); i++) {
          switch (op) {
            case "*":
              r = Algebra.prodotto(r, matrici.get(i));
              break;
            case "+":
              r = Algebra.somma(r, matrici.get(i));
              break;
            case "-":
              r = Algebra.differenza(r, matrici.get(i));
              break;
          }
        }
        matrici.add(r);
        desk.addElem(new Elem(new_id, r));
        if (!new_id.equals("")) {
          new_id = "<b>" + new_id + "</b> = ";
        }
        desk.add(new_id + s + " = ", matrici, op, numCifre);
        reset();
      } else {
        desk.add("<font color='#163279'>" + tab + "> Impossibile effettuare questo tipo"
                + " di operazione tutte le matrici devono avere lo stesso numero di righe/colonne</font>");
      }
    } else {
      desk.add("<font color='#163279'>" + tab + "> Attenzione una o più matrici potrebbero non esistere</font>");
    }
  }

  private static double parseValue(String value) {
    if (value.matches(Generici.INR)) {
      String membri[] = value.split("\\/", 2);
      return (Double.valueOf(membri[0]) / Double.valueOf(membri[1]));
    } else {
      return Double.valueOf(value);
    }
  }

  @Override
  public void itemStateChanged(ItemEvent ie) {
    JRadioButtonMenuItem rb = (JRadioButtonMenuItem) ie.getItem();
    numCifre = Integer.valueOf(rb.getActionCommand());
  }

  private JMenuBar creaMenuBar() {
    JMenuBar barraDeiMenu = new JMenuBar();
    JMenu menu1 = new JMenu("File");
    JMenuItem menuItem0 = new JMenuItem("Nuovo progetto");
    JMenuItem menuItem00 = new JMenuItem("Apri progetto");
    JMenuItem menuItemSalva = new JMenuItem("Salva");
    JMenuItem menuItemSalvaN = new JMenuItem("Salva con nome");
    JMenuItem menuItem1 = new JMenuItem("Esci");
    menuItem0.addActionListener(this);
    menuItem1.addActionListener(this);
    menuItem00.addActionListener(this);
    menuItemSalva.addActionListener(this);
    menuItemSalvaN.addActionListener(this);
    menu1.add(menuItem0);
    menu1.add(menuItem00);
    menu1.addSeparator();
    menu1.add(menuItemSalva);
    menu1.add(menuItemSalvaN);
    menu1.addSeparator();
    menu1.add(menuItem1);
    ButtonGroup itemGroup = new ButtonGroup();
    JMenu menuTroncamento = new JMenu("Troncamento");
    JRadioButtonMenuItem item[] = new JRadioButtonMenuItem[10];
    for (int i = 0; i < 10; i++) {
      item[i] = new JRadioButtonMenuItem(String.valueOf(i + 1));
      item[i].addItemListener(this);
      itemGroup.add(item[i]);
      menuTroncamento.add(item[i]);
    }
    item[2].setSelected(true);
    JMenu menu2 = new JMenu("?");
    JMenuItem menuItem2 = new JMenuItem("Info");
    menuItem2.addActionListener(this);
    menu2.add(menuItem2);
    barraDeiMenu.add(menu1);
    barraDeiMenu.add(menuTroncamento);
    barraDeiMenu.add(menu2);
    return barraDeiMenu;
  }

  private void initUI() {

    tabPane = new JTabbedPane();
    //tabPane.setAutoscrolls(true);
    tabPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    this.getContentPane().add("Center", tabPane);
    btnOk = new JButton("Vai");
    btnOk.addActionListener(this);
    txtExpr = new JTextField();
    txtExpr.setColumns(50);
    txtExpr.setFont(new Font("Arial", Font.PLAIN, 18));
    InputMap inputMap = txtExpr.getInputMap();
    KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK);
    inputMap.put(key, DefaultEditorKit.backwardAction);
    JLabel l = new JLabel("Edit: ", JLabel.CENTER);
    l.setFont(new Font("Arial", Font.PLAIN, 18));
    JPanel p = new JPanel();
    p.setLayout(new FlowLayout());
    p.add(l);
    p.add(txtExpr);
    p.add(btnOk);
    this.getContentPane().add("South", p);
  }

  private String creaHTMLPolinomio(String str, double coef[]) {
    String pol = "<b> " + tab + " P(" + str + ") <small>=</small>  X<sup>" + (coef.length - 1) + "</sup>";
    for (int i = coef.length - 2; i > 0; i--) {
      //pol += Algebra.tronca(numCifre, coef[i]) + " * X<sup>" + i + "</sup> ";
      if (coef[i] >= 0) {
        pol += "<small> + </small>" + Algebra.tronca(numCifre, coef[i]) + " <small>*</small> X<sup>" + i + "</sup> ";
      } else {
        pol += "<small> - </small>" + Algebra.tronca(numCifre, Math.abs(coef[i])) + " <small>*</small> X<sup>" + i + "</sup> ";
      }
    }
    if (coef[0] >= 0) {
      pol += "<small> + </small>" + Algebra.tronca(numCifre, coef[0]);
    } else {
      pol += "<small> - </small>" + Algebra.tronca(numCifre, Math.abs(coef[0]));
    }
    pol += "</b>";
    return pol;
  }

  private String creaHTMLAutoValori(double autoValori[]) {
    String html = "<b> " + tab;
    for (int i = 0; i < autoValori.length; i++) {
      html += "  &lambda;<sub><small>" + i + "</small></sub><small>=</small> " + Algebra.tronca(numCifre, autoValori[i]) + "; &nbsp&nbsp ";
    }
    html += "</b>";
    return html;
  }
}
