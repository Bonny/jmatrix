package jmatrix;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.text.html.HTMLEditorKit;

public class FinestraInfo extends JFrame {

  private JEditorPane editor;

  public FinestraInfo() {
    super("Info");

    editor = new JEditorPane();
    editor.setEditable(false);
    editor.setEditorKit(new HTMLEditorKit());
    editor.setBackground(Color.white);

    editor.setText(testo);

    this.getContentPane().add("Center", new JScrollPane(editor));

    this.setVisible(false);
    this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    this.setLocation((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - 350, (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2 - 250);
    this.setSize(900, 500);
    this.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        setVisible(false);
      }
    });
  }
  final String testo = "Questo software contiene quasi tutto il programma di Algebra lineare di un corso universitario. Ho cercato di emulare, in parte, il software molto conosciuto dalle scuole superiori secondarie Derive 6."
          + "Il programma si presenta con una finestra simile ad una lavagna ed un'area di testo, dove vengono inserite le matrici (nxm) ed in comandi per effettuare operazioni su o tra di esse."
          + "Vediamo una spiegazione dettagliata su come lavorare con JMatrix."
          + "<br>I comandi da passare a JMatrix si suddividono in tre categorie<br>"
          + "<ul>"
          + "<li><em>Generici</em>: i numeri possono essere rappresentati come un intero,un numero in virgola mobile e numeri inrazionali (es 1/2).</li>"
          + "<li><em>Dichiarazioni:</em> una matrice deve essere dichiarata rispettando in seguente pattern NOME=[(x<sub>1</sub>,....,x<sub>n</sub>)....(x<sub>1</sub>,....,x<sub>n</sub>)] x deve appartenere all'insieme de Generici.</li>"
          + "<li><em>Comandi</em>: con le matrici si possono effettuare diverse operazioni per esempio calcolare la trasposta, l'inversa ecc... i comandi servono a questo scopo.</li>"
          + "</ul>"
          + "<br>Vediamo ora la specifica di ogni comando (M sta per una generica matrice):<br>"
          + "<ul>"
          + "<li><strong>SYS(M)</strong>: data una matrice M calcola le soluzioni del sistema lineare associato alla matrice M, se la soluzione è unica si calcola mediante la regola di Cramer altrimenti con la regola di Rouché-Capelli.</li>"
          + "<li><strong>INV(M)</strong>: data una matrice ne calcola l'inversa M<sup>-1</sup>.</li>"
          + "<li><strong>TRS(M)</strong>: data una matrice M ne calcola la matrice trasposta M<sub>t</sub>.</li>"
          + "<li><strong>RG(M)</strong>: data una matrice M ne calcola il rango mediante la regola dei minori.</li>"
          + "<li><strong>DET(M)</strong>: data una matrice M ne calcola il determinante mediante la regola di Laplace.</li>"
          + "<li><strong>TR(M)</strong>: data una matrice M ne calcola la traccia.</li>"
          + "<li><strong>M<sub>1</sub> - .... - M<sub>n</sub></strong>: date una serie di matrici M<sub>n</sub> separate dall'operatore - ne calcola la matrice differenza.</li>"
          + "<li><strong>M<sub>1</sub> + .... + M<sub>n</sub></strong>: date una serie di matrici M<sub>n</sub> separate dall'operatore + ne calcola la matrice somma.</li>"
          + "<li><strong>M<sub>1</sub> * .... * M<sub>n</sub></strong>: date una serie di matrici M<sub>n</sub> separate dall'operatore * ne calcola il prodotto di matrici.</li>"
          + "<li><strong>M<sub>1</sub> * .... * g</strong>: g sta per generico ovvero il prodotto di una matrice per uno scalare.</li>"
          + "<li><strong>P(M)</strong> : data una matrice M ne calcola il polinomio caratteristico.</li>"
          + "<li><strong>AVL(M)</strong>: data una matrice M ne calcola gli auto valori (anche Complessi).</li>"
          + "</ul>"
          + "<br>La dichiarazione di una matrice può avvenire attraverso la combinazione di comandi per esempio:"
          + "<br><br>"
          + "<pre>M = [(2,5/7,9)(1/2,0,7)(1,1,12.3)]</pre>"
          + "<br>"
          + "<pre>T = TRS(M) | INV(M) | M<sub>1</sub> * .... * M<sub>n</sub>  |  M<sub>1</sub> + .... + M<sub>n</sub>  |  M</pre><sub>1</sub> - .... - M<sub>n</sub></pre>"
          + "<br><br>"
          + "e cosi via...<br><br>"
          + "JMatrix offre la possibilità di lavorare a più progetti (schede) e di salvare e aprire progetti già esistenti."
          + "Questo programma l'ho creato per gli appassionati come me di matematica per avere un programma semplice per testare le soluzioni degli esercizi."
          + "<br><br>Buon divertimento!!<br>";
}
