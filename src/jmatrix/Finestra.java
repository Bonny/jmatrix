package jmatrix;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 * @author bonny
 */
public class Finestra extends JFrame implements ActionListener {

    private ArrayList<Progetto> arrayList;
    private JList list;

    /**
     * 
     */
    public Finestra() {
        super("Save");
        arrayList = new ArrayList<Progetto>();
        list = new JList();
        list.setPreferredSize(new Dimension(250, 400));
        JScrollPane scp = new JScrollPane(list);
        JPanel pl = new JPanel();
        pl.add(scp);
        pl.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.getContentPane().add("West", pl);

        JPanel p = new JPanel();
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints lim = new GridBagConstraints();
        p.setLayout(layout);

        JButton btnSalva = new JButton("Salva");
        btnSalva.addActionListener(this);
        lim.gridx = 0;
        lim.gridy = 0;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.weighty = 1;
        layout.setConstraints(btnSalva, lim);
        p.add(btnSalva);

        JButton btnSalvaAll = new JButton("Salva Tutto");
        btnSalvaAll.addActionListener(this);
        lim.gridx = 0;
        lim.gridy = 1;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.weighty = 0;
        layout.setConstraints(btnSalvaAll, lim);
        p.add(btnSalvaAll);

        JButton btnNoSalva = new JButton("Non Salvare");
        btnNoSalva.addActionListener(this);
        lim.gridx = 0;
        lim.gridy = 2;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.weighty = 1;
        layout.setConstraints(btnNoSalva, lim);
        p.add(btnNoSalva);

        JButton btnAnnulla = new JButton("Annulla");
        btnAnnulla.addActionListener(this);
        lim.gridx = 0;
        lim.gridy = 3;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.weighty = 2;
        layout.setConstraints(btnAnnulla, lim);
        p.add(btnAnnulla);

        p.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        this.getContentPane().add("East", p);
        this.setVisible(false);
        Toolkit mioToolkit = Toolkit.getDefaultToolkit();
        Dimension dimSchermo = mioToolkit.getScreenSize();
        this.setLocation((int) dimSchermo.getWidth() / 2 - 150, (int) dimSchermo.getHeight() / 2 - 150);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        });
        this.pack();

    }

    public void actionPerformed(ActionEvent ae) {
        String e = ae.getActionCommand();
        if (e.equals("Non Salvare")) {
            System.exit(0);
        } else if (e.equals("Annulla")) {
            this.setVisible(false);
        } else if (e.equals("Salva Tutto")) {
            ArrayList<Progetto> ris = new ArrayList<Progetto>();
            for (int index = 0; index < arrayList.size(); index++) {
                Progetto p = arrayList.get(index);
                if (p.getBT().getFile() == null) {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setDialogTitle("ciao");
                    if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                        p.getBT().setFile(new File(chooser.getSelectedFile().getAbsolutePath() + ".alg"));
                        ApriSalvaProgetto.salva(p.getDesk(), p.getBT().getFile());
                        p.getBT().setText(p.getBT().getFile().getName());
                        p.getBT().setStato(true);
                    } else {
                        ris.add(p);
                    }
                } else {
                    ApriSalvaProgetto.salva(p.getDesk(), p.getBT().getFile());
                    p.getBT().setText(p.getBT().getFile().getName());
                    p.getBT().setStato(true);
                }
            }
            if (ris.isEmpty()) {
                System.exit(0);
            } else {
                arrayList.clear();
                arrayList = ris;
                this.updateJList();
            }
        } else if (e.equals("Salva")) {
            //System.out.println(list.getSelectedIndex());
            int index = list.getSelectedIndex();
            if (index >= 0) {
                Progetto p = arrayList.get(index);
                if (p.getBT().getFile() == null) {

                    JFileChooser chooser = new JFileChooser();
                    chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));
                    chooser.setMultiSelectionEnabled(false);

                    if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {

                        p.getBT().setFile(new File(chooser.getSelectedFile().getAbsolutePath() + ".alg"));
                        ApriSalvaProgetto.salva(p.getDesk(), p.getBT().getFile());
                        p.getBT().setText(p.getBT().getFile().getName());
                        p.getBT().setStato(true);
                        arrayList.remove(index);
                        this.updateJList();
                        if (arrayList.isEmpty()) {
                            System.exit(0);
                        }
                    } else {
                        return;
                    }

                } else {
                    ApriSalvaProgetto.salva(p.getDesk(), p.getBT().getFile());
                    p.getBT().setText(p.getBT().getFile().getName());
                    p.getBT().setStato(true);
                    arrayList.remove(index);
                    this.updateJList();
                    if (arrayList.isEmpty()) {
                        System.exit(0);
                    }
                }
            }
        }
    }

    /**
     * 
     * @param pane
     */
    public void init(JTabbedPane pane) {
        arrayList.clear();
        arrayList.trimToSize();

        for (int i = 0; i < pane.getTabCount(); i++) {
            pane.setSelectedIndex(i);
            ButtonTab bt = (ButtonTab) pane.getTabComponentAt(i);
            if (!bt.getStato()) {
                ScrollDesk sc = (ScrollDesk) pane.getSelectedComponent();
                arrayList.add(new Progetto(bt, sc.getDesk()));
            }
            //System.out.println(i);
        }
        updateJList();

    }

    /**
     * 
     */
    public void updateJList() {

        String str[] = new String[arrayList.size()];
        int i = 0;
        for (Progetto p : arrayList) {
            str[i++] = p.toString();
        }
        list.setListData(str);
    }

    private class Progetto {

        private ButtonTab bt;
        private Desk desk;

        public Progetto(ButtonTab bt, Desk desk) {
            this.bt = bt;
            this.desk = desk;
        }

        public ButtonTab getBT() {
            return this.bt;
        }

        public Desk getDesk() {
            return this.desk;
        }

        @Override
        public String toString() {
            return this.bt.getText();
        }
    }
}
