package jmatrix;

/********************************************************************************
 * Copyright (C) 19aa Luca Bonaldo alias Bonny                          *
 * Questo programma è software libero; è lecito ridistribuirlo e/o      *
 * modificarlo secondo i termini della Licenza Pubblica Generica GNU    *
 * come pubblicata dalla Free Software Foundation; o la versione 2      *
 * della licenza o (a scelta) una versione successiva.                    *
 *                                                                              *
 * Questo programma è distribuito  nella speranza che sia utile, ma             *
 * SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di                *
 * COMMERCIABILITÀ o di APPLICABILITÀ PER UN PARTICOLARE SCOPO. Si              *
 * veda la Licenza Pubblica Generica GNU per avere maggiori dettagli.           *
 *                                                                              *
 * Ognuno dovrebbe avere ricevuto una copia  della Licenza Pubblica             *
 * Generica GNU insieme a questo programma; in caso contrario, la si            *
 * può ottenere dalla Free Software Foundation, Inc., 675 Mass Ave,             *
 * Cambridge, MA 02139, Stati Uniti.                                            *
 *                                                                              *
 * Per contattarmi attraverso posta elettronica: info@lucabonaldo.it           *
 ********************************************************************************/
/**
 * 
 * @author bonny
 */
public class Main {

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        MyFrame myFrame = new MyFrame();

    }
}
