package jmatrix;

import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTMLEditorKit;

/**
 * 
 * @author bonny
 */
public class SubDesk extends JEditorPane {

    /**
     * 
     */
    public SubDesk() {
        super();
        this.setEditable(false);
        this.setEditorKit(new HTMLEditorKit());
        this.setBackground(Color.white);
    }
}
