/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmatrix;

import javax.swing.JScrollPane;

/**
 * 
 * @author bonny
 */
public class ScrollDesk extends JScrollPane {

    private Desk desk;

    /**
     * 
     * @param desk
     */
    public ScrollDesk(Desk desk) {
        super(desk);
        this.desk = desk;
    }

    /**
     * 
     * @return
     */
    public Desk getDesk() {
        return this.desk;
    }
}
