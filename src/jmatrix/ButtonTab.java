package jmatrix;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * 
 * @author bonny
 */
public class ButtonTab extends JPanel {

    private File file = null;
    private boolean isStato = false;
    private JTabbedPane pane;
    private JLabel label;

    /**
     * 
     * @param pane
     */
    public ButtonTab(final JTabbedPane pane) {

        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.pane = pane;
        setOpaque(false);


        label = new JLabel() {

            @Override
            public String getText() {
                int i = pane.indexOfTabComponent(ButtonTab.this);
                if (i != -1) {
                    return pane.getTitleAt(i);
                }
                return null;
            }

            @Override
            public void setText(String str) {
                int i = pane.indexOfTabComponent(ButtonTab.this);
                if (i != -1) {
                    pane.setTitleAt(i, str);
                }
            }
        };

        label.setFont(new Font("Arial", Font.PLAIN, 14));
        add(label);
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        JButton button = new TabButton();
        add(button);
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

    }

    /**
     * 
     * @param file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * 
     * @return
     */
    public File getFile() {
        return this.file;
    }

    /**
     * 
     * @param str
     */
    public void setText(String str) {
        this.label.setText(str);
    }

    /**
     * 
     * @return
     */
    public String getText() {
        return this.label.getText();
    }

    /**
     * 
     * @return
     */
    public boolean getStato() {
        return this.isStato;
    }

    /**
     * 
     * @param flag
     */
    public void setStato(boolean flag) {
        this.isStato = flag;
        if (!this.isStato) {
            label.setFont(new Font("Arial", Font.BOLD, 13));
        } else {
            label.setFont(new Font("Arial", Font.PLAIN, 14));
        }
    }

    private class TabButton extends JButton implements ActionListener {

        public TabButton() {
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("chiudi questo tab");
            setUI(new BasicButtonUI());
            setContentAreaFilled(false);
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            addMouseListener(buttonMouseListener);
            setRolloverEnabled(true);
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int i = pane.indexOfTabComponent(ButtonTab.this);
            if (i != -1) {
                if (isStato) {
                    pane.remove(i);
                    pane.updateUI();
                } else {
                    if (file == null ) {

                        final int risp = JOptionPane.showConfirmDialog(this,
                                "Salvare il progetto?", "Avviso",
                                JOptionPane.YES_NO_CANCEL_OPTION);

                        if (risp == 0) {
                            JFileChooser chooser = new JFileChooser();
                            chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));
                            chooser.setMultiSelectionEnabled(false);

                            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {

                                file = new File(chooser.getSelectedFile().getAbsolutePath() + ".alg");
                                ScrollDesk sc = (ScrollDesk) pane.getSelectedComponent();
                                ApriSalvaProgetto.salva(sc.getDesk(), file);
                                pane.remove(i);
                                pane.updateUI();
                            } else {
                                return;
                            }
                        } else if (risp == 1) {
                            pane.remove(i);
                            pane.updateUI();
                        }
                    } else {
                        final int risp = JOptionPane.showConfirmDialog(this,
                                "Salvare il progetto?", "Avviso",
                                JOptionPane.YES_NO_CANCEL_OPTION);
                        if (risp == 0) {
                            ScrollDesk sc = (ScrollDesk) pane.getSelectedComponent();
                            ApriSalvaProgetto.salva(sc.getDesk(), file);
                            pane.remove(i);
                            pane.updateUI();
                        } else if (risp == 1) {
                            pane.remove(i);
                            pane.updateUI();
                        }

                    }
                }
            }
        }

        @Override
        public void updateUI() {
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();

            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }
            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.BLACK);
            /*
            if (getModel().isRollover()) {
            g2.setColor(Color.red);
            }
             * 
             */
            int delta = 6;
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
            g2.dispose();
        }
    }
    private final static MouseListener buttonMouseListener = new MouseAdapter() {

        @Override
        public void mouseEntered(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(true);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(false);
            }
        }
    };
}
