package jmatrix;

import java.io.*;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import modello.Generici;

/**
 *
 * @author bonny
 */
public class ApriSalvaProgetto {

    /**
     *
     * @param tabPane
     */
    public static void apri(JTabbedPane tabPane) {

        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("alg file", "alg"));

        if (chooser.showOpenDialog(tabPane) == JFileChooser.APPROVE_OPTION) {
            try {
                String s = null, progetto = "";
                File file = chooser.getSelectedFile();
                BufferedReader fin = new BufferedReader(new FileReader(file));

                s = fin.readLine();
                while (s != null) {

                    progetto += s;
                    s = fin.readLine();
                }

                String p[] = progetto.split("\\$", 2);

                if (p[1].matches(Generici.MATRICI) || p[1].equals("")) {

                    ArrayList<Elem> lista = new ArrayList<Elem>();

                    if (!p[1].equals("")) {
                        String matrici[] = p[1].split(";");
                        for (String o : matrici) {
                            String v[] = o.split("=");
                            double m[][] = MyFrame.parseMatrice(v[1]);
                            if (m != null) {
                                lista.add(new Elem(v[0], m));
                            }
                        }
                    }
                    Desk desk = new Desk(p[0], lista, tabPane);
                    tabPane.addTab(file.getName(), new ScrollDesk(desk));
                    int c = tabPane.getTabCount() - 1;
                    ButtonTab bt = new ButtonTab(tabPane);
                    bt.setFile(file);
                    bt.setStato(true);
                    tabPane.setTabComponentAt(c, bt);
                    tabPane.setSelectedIndex(c);
                } else {
                    JOptionPane.showMessageDialog(null, "Impossibile aprire il progetto"
                            + "\nil file potrebbe essere danneggiato ", "Errore", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Impossibile aprire il progetto"
                        + "\nTipo errore: " + ex.getMessage(), "Errore", JOptionPane.WARNING_MESSAGE);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Impossibile aprire il progetto"
                        + "\nTipo errore: " + ex.getMessage(), "Errore", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     *
     * @param d
     * @param file
     */
    public static void salva(Desk d, File file) {

        try (PrintWriter fout = new PrintWriter(new FileWriter(file))) {
            String s = d.getProgetto();
            fout.print(s);
            fout.println("$");
            fout.flush();
            ArrayList<Elem> lista = d.getListElem();
            for (int k = 0; k < lista.size(); k++) {
                s = "";
                s += lista.get(k).getNome() + "=[";
                double m[][] = lista.get(k).getMatrice();
                for (int i = 0; i < m.length; i++) {
                    s += "(";
                    for (int j = 0; j < m[0].length; j++) {
                        s += String.valueOf(m[i][j]);
                        if (j < m[0].length - 1) {
                            s += ",";
                        }
                    }
                    s += ")";
                }
                s += "]";
                if (k < lista.size() - 1) {
                    s += ";";
                }
                fout.println(s);
            }
            fout.flush();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Impossibile salvare il progetto"
                    + "\nTipo errore: " + ex.getMessage(), "Errore", JOptionPane.WARNING_MESSAGE);
        }
    }
}
