package algebra;

/*
 * k indice che scorre le colonne di M (M[0][k]) con  0 <= k < col
 * ris contiene la somma data dalla regola di Laplace
 * Primo teorema di Laplace: il determinante di una matrice quadrata A è pari alla somma dei prodotti degli elementi
 * di una riga qualsiasi per i rispettivi complementi algebrici.
 * ris = det(M) = somma[i=0 To n-1]{ segno(i)*M[0][i]*det(m(i)) + .... + segno(i+1)*M[0][i+1]*det(m(i+1)) + .... }
 * dove segno(i) = (-1)^(i+j) ; i,j indici della matrice M
 */
/**
 *
 * @author bonny
 */
public class Determinante {

    /**
     * 
     * @param M
     * @return
     */
    public static double det(double M[][]) {
        int i, j, k;
        final int row = M.length, col = M[0].length;

        if (row == 2 && col == 2) {
            return detMin(M);
        } else if (row > 2 && col > 2) {

            double m[][];
            double r = 0;

            for (k = 0; k < col; k++) { // ..vai alla colonna M[0][k] ..

                m = new double[row - 1][col - 1];
                //prendo da M tutti i vettori colonna con indice j , per 0 <= j < k
                for (j = 0; j < k; j++) {
                    for (i = 1; i < row; i++) {
                        m[i - 1][j] = M[i][j];
                    }
                }
                //analogamente per k < j < col
                for (j = k + 1; j < col; j++) {
                    for (i = 1; i < row; i++) {
                        m[i - 1][j - 1] = M[i][j];
                    }
                }

                r += (double) segn(k) * M[0][k] * det(m);
            }
            return r;
        } else {
            return M[0][0];
        }
    }

    private static double detMin(double min[][]) {
        return ((min[0][0] * min[1][1]) - (min[0][1] * min[1][0]));
    }

    private static int segn(int exp) {
        return (int) (Math.pow(-1, exp));
    }
}
