package algebra;


/**
 *
 * @author bonny
 */
public class Traccia {


    /**
     * 
     * @param M
     * @return
     */
    public static double traccia(double M[][]) {
        int i, j;
        final int row = M.length, col = M[0].length;
        double tr = 0;
        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                if (i == j) {
                    tr += M[i][j];
                }
            }
        }
        return tr;
    }
}
