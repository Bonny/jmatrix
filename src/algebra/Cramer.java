package algebra;

/**
 *
 * @author bonny
 */
public class Cramer {

    /**
     * 
     * @param A
     * @param B
     * @return
     */
    public static double[] RisolviSistema(double A[][], double B[]) {

        /*s sontiene la ennupla ovvero la soluzione del sistema*/
        double s[] = null;

        if (A.length == A[0].length) {

            double detA = Determinante.det(A);

            if (detA != 0) {

                final int dim = A.length;

                s = new double[dim];

                double T[][] = new double[dim][dim];

                /*per ogni colonna*/
                for (int i = 0; i < dim; i++) {

                    /*inserisco in T la colonna dei termininoti B*/
                    for (int k = 0; k < dim; k++) {
                        T[k][i] = B[k];
                    }

                    /*inserisco in T in complemento algebrico di A(i)*/
                    for (int j = 0; j < i; j++) {
                        for (int h = 0; h < dim; h++) {
                            T[h][j] = A[h][j];
                        }
                    }

                    for (int j = i + 1; j < dim; j++) {
                        for (int h = 0; h < dim; h++) {
                            T[h][j] = A[h][j];
                        }
                    }
                    /*fine comp alg*/

                    s[i] = Determinante.det(T) / detA;
                }
            }
        }
        return s;
    }
}
