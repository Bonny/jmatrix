package algebra;

/**
 *
 * @author bonny
 */
public class Polinomio {

    /**
     * 
     * @param M
     * @return
     */
    public static double[] crea(double M[][]) {

        double coef[] = null;

        if (M.length == M[0].length) {


            final int n = M.length;

            if (n > 1 && n < 4) {

                coef = new double[n + 1];

                switch (n) {

                    case 2:

                        coef[2] = 1.0; //x^2
                        coef[1] = (-1.0) * Traccia.traccia(M); // -tr(M) * x
                        coef[0] = Determinante.det(M); //termine noto
                        break;

                    case 3:

                        coef[3] = -1.0; //x^3

                        coef[2] = Traccia.traccia(M); // +tr(M) * x^2

                        double z = 0;
                        double minore[][];

                        minore = new double[n - 1][n - 1];
                        minore[0][0] = M[0][0];
                        minore[0][1] = M[0][1];
                        minore[1][0] = M[1][0];
                        minore[1][1] = M[1][1];
                        z += Determinante.det(minore);
//Algebra.stampa(minore); System.out.println();
                        minore = new double[n - 1][n - 1];
                        minore[0][0] = M[1][1];
                        minore[0][1] = M[1][2];
                        minore[1][0] = M[2][1];
                        minore[1][1] = M[2][2];
                        z += Determinante.det(minore);
//Algebra.stampa(minore); System.out.println();
                        minore = new double[n - 1][n - 1];
                        minore[0][0] = M[0][0];
                        minore[0][1] = M[0][2];
                        minore[1][0] = M[2][0];
                        minore[1][1] = M[2][2];
//Algebra.stampa(minore); System.out.println();
                        z += Determinante.det(minore);
                        coef[1] = z;

                        coef[0] = Determinante.det(M);//termine noto
/*
                         * for (int i = 0; i < coef.length; i++) { coef[i] =
                         * (-1.0) * coef[i]; }
                         *
                         */
                        break;
                }

            }
        }

        return coef;
    }

    /**
     * 
     * @param coef
     * @return
     */
    public static double[] risolvi_g2(double coef[]) {

        double radici[] = null;

        if (coef.length == 3) {

            if (coef[2] != 0) {

                if (coef[0] == 0 && coef[1] != 0) {          /*
                     * eq spuria
                     */
                    radici = new double[2];
                    radici[0] = 0;
                    radici[1] = (-1.0) * coef[1] / coef[2];
                } else if (coef[0] != 0 && coef[1] == 0) {   /*
                     * eq pura
                     */
                    double d = (-1.0) * coef[0] / coef[2];
                    if (d > 0) {
                        radici = new double[2];
                        d = Math.sqrt(d);
                        radici[0] = (-1.0) * d;
                        radici[1] = d;
                    }
                } else if (coef[0] == 0 && coef[1] == 0) {   /*
                     * eq monomia
                     */
                    radici = new double[1];
                    radici[0] = 0;
                } else if (coef[0] != 0 && coef[1] != 0) {     /*
                     * eq completa
                     */
                    double delta = (coef[1] * coef[1]) - 4.0 * coef[0];
                    if (delta >= 0) {
                        radici = new double[2];
                        radici[0] = (((-1.0) * coef[1]) - Math.sqrt(delta)) / (2.0 * coef[2]);
                        radici[1] = (((-1.0) * coef[1]) + Math.sqrt(delta)) / (2.0 * coef[2]);
                    }
                }
            }
        }
        return radici;

    }

    /**
     * 
     * @param coef
     * @return
     */
    public static Complesso[] risolvi_g3(double coef[]) {

        Complesso c[] = new Complesso[3];
        for (int i = 0; i < 3; i++) {
            c[i] = new Complesso();
        }
        double discrim, q, r, dum1, s, t, term1, r13;
        coef[2] /= coef[3];
        coef[1] /= coef[3];
        coef[0] /= coef[3];

        q = (3.0 * coef[1] - (coef[2] * coef[2])) / 9.0;
        r = -(27.0 * coef[0]) + coef[2] * (9.0 * coef[1] - 2.0 * (coef[2] * coef[2]));
        r /= 54.0;

        discrim = q * q * q + r * r;
        term1 = (coef[2] / 3.0);

        c[0].setIm(0);

        if (discrim > 0) {
            s = r + Math.sqrt(discrim);
            s = ((s < 0) ? -Math.pow(-s, (1.0 / 3.0)) : Math.pow(s, (1.0 / 3.0)));
            t = r - Math.sqrt(discrim);
            t = ((t < 0) ? -Math.pow(-t, (1.0 / 3.0)) : Math.pow(t, (1.0 / 3.0)));
            c[0].setRe(-term1 + s + t);
            term1 += (s + t) / 2.0;
            c[1].setRe(-term1);
            c[2].setRe(-term1);
            term1 = Math.sqrt(3.0) * (-t + s) / 2;
            c[1].setIm(term1);
            c[2].setIm(-term1);
            return c;
        }

        if (discrim == 0) {
            r13 = ((r < 0) ? -Math.pow(-r, (1.0 / 3.0)) : Math.pow(r, (1.0 / 3.0)));
            c[0].setRe(-term1 + 2.0 * r13);
            c[1].setRe(-(r13 + term1));
            c[2].setRe(-(r13 + term1));
            return c;
        }
    
        q = -q;
        dum1 = q * q * q;
        dum1 = Math.acos(r / Math.sqrt(dum1));
        r13 = 2.0 * Math.sqrt(q);
        c[0].setRe(-term1 + r13 * Math.cos(dum1 / 3.0));
        c[1].setRe(-term1 + r13 * Math.cos((dum1 + 2.0 * Math.PI) / 3.0));
        c[2].setRe(-term1 + r13 * Math.cos((dum1 + 4.0 * Math.PI) / 3.0));

        return c;
    }
    

}
