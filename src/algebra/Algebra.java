package algebra;

import java.math.BigDecimal;

/**
 *
 * @author bonny
 */
public class Algebra {

  /**
   *
   * @param a
   * @param b
   * @return
   */
  public static double[][] somma(double a[][], double b[][]) {
    return (Somma.somma(a, b));
  }

  /**
   *
   * @param a
   * @param b
   * @return
   */
  public static double[][] prodotto(double a[][], double b[][]) {
    return (Prodotto.prodotto(a, b));
  }

  /**
   *
   * @param a
   * @param b
   * @return
   */
  public static double[][] prodotto(double a, double b[][]) {
    return (Prodotto.prodotto(a, b));
  }

  /**
   *
   * @param a
   * @param b
   * @return
   */
  public static double[][] differenza(double a[][], double b[][]) {
    return (Differenza.differenza(a, b));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double det(double M[][]) {
    return (Determinante.det(M));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double[][] inversa(double M[][]) {
    return (Inversa.inversa(M));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double[][] trasposta(double M[][]) {
    return (Trasposta.trasposta(M));
  }

  /**
   *
   * @param M
   * @return
   */
  public static int rango(double M[][]) {
    return (Rango.rango(M));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double traccia(double M[][]) {
    return (Traccia.traccia(M));
  }

  /**
   *
   * @param cifre
   * @param val
   * @return
   */
  public static double tronca(int cifre, double val) {
    BigDecimal bg = new BigDecimal(val);
    bg = bg.setScale(cifre, BigDecimal.ROUND_HALF_UP);
    return bg.doubleValue();
  }

  /**
   *
   * @param M
   * @return
   */
  public static int sysRCapelli(double M[][]) {
    return (RCapelli.risolviSistema(M));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double[] sysCramer(double M[][]) {

    double A[][] = new double[M.length][M[0].length - 1];
    double B[] = new double[M.length];

    for (int i = 0; i < M.length; i++) {
      B[i] = M[i][M[0].length - 1];
      for (int j = 0; j < M[0].length - 1; j++) {
        A[i][j] = M[i][j];
      }

    }

    return (Cramer.RisolviSistema(A, B));
  }

  /**
   *
   * @param M
   * @return
   */
  public static double[] polinomio(double M[][]) {
    return (Polinomio.crea(M));
  }

  public static double[] risolvi_g2(double coef[]) {
    return (Polinomio.risolvi_g2(coef));
  }

  public static Complesso[] risolvi_g3(double coef[]) {
    return (Polinomio.risolvi_g3(coef));
  }

  /**
   *
   * @param M
   */
  public static void stampa(double M[][]) {
    int i, j;
    final int row = M.length, col = M[0].length;
    for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {

        System.out.print(M[i][j] + "  ");

        if (j == col - 1) {
          System.out.print("\n");
        }
      }
    }
  }
}
