package algebra;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bonny
 */
public class Rango {

    private static int i, j, h, k;


    /**
     * 
     * @param M
     * @return
     */
    public static int rango(double M[][]) {

        int r = 0;
        final int row = M.length, col = M[0].length;
        final int min = min(row, col);
        r = rango(M, min);
        return r;
    }

    private static int rango(double M[][], int min) {

        if (min == 1) {
            return 1;
        } else {

            double m[][] = null;
            final int row = M.length, col = M[0].length;
            List<Double> d = new ArrayList();

            for (i = 0; i <= row - min; i++) {

                j = 0;

                for (; j <= col - min; j++) {

                    m = new double[min][min];

                    for (k = i; k < min + i; k++) {

                        for (h = j; h < min + j; h++) {

                            m[k - i][h - j] = M[k][h];

                        }
                    }
                    d.add(Determinante.det(m));
                }
            }

            i = 0;
            boolean flag = false;
            while (i < d.size() && !flag) {
                if (d.get(i) != 0) {
                    flag = true;
                }
                i++;
            }

            if (flag) {
                return 1 + rango(M, min - 1);
            } else {
                return 0 + rango(M, min - 1);
            }

        }
    }

    private static int min(int a, int b) {
        if (a <= b) {
            return a;
        } else {
            return b;
        }
    }
}
