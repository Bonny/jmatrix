package algebra;

/**
 *
 * @author bonny
 */
public class Inversa {

  /**
   *
   * @param M
   * @return
   */
  public static double[][] inversa(double M[][]) {

    int i, j, k, h;
    final double detM = Determinante.det(M);

    if (detM == 0) {
      return null;
    } else {

      final int row = M.length, col = M[0].length;
      double trasp[][] = Trasposta.trasposta(M);
      double inversa[][] = new double[row][col];
      double m[][] = new double[row - 1][col - 1];

      for (k = 0; k < row; k++) {
        for (h = 0; h < col; h++) {

          for (j = 0; j < h; j++) {
            for (i = 0; i < k; i++) {
              m[i][j] = trasp[i][j];
            }
          }

          for (j = h + 1; j < col; j++) {
            for (i = 0; i < k; i++) {
              m[i][j - 1] = trasp[i][j];
            }
          }

          for (j = 0; j < h; j++) {
            for (i = k + 1; i < row; i++) {
              m[i - 1][j] = trasp[i][j];
            }
          }

          for (j = h + 1; j < col; j++) {
            for (i = k + 1; i < row; i++) {
              m[i - 1][j - 1] = trasp[i][j];
            }
          }

          inversa[k][h] = (double) (Math.pow(-1, k + h) * Determinante.det(m)) / detM;
        }
      }
      return inversa;
    }
  }
}
