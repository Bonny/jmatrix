package algebra;

/**
 *
 * @author bonny
 */
public class RCapelli {

    /**
     * 
     * @param M
     * @return
     */
    public static int risolviSistema(double M[][]) {

        int s;
        double A[][] = new double[M.length][M[0].length - 1];

        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[0].length - 1; j++) {
                A[i][j] = M[i][j];
            }
        }

        int rangoA = Rango.rango(A);

        int rangoM = Rango.rango(M);

        if (rangoA == rangoM) {
            return ((int) M.length - rangoM);
        } else {
            return -1;
        }

    }
}
