package algebra;

/**
 *
 * @author bonny
 */
public class Somma {

    /**
     * 
     * @param a
     * @param b
     * @return
     */
    public static double[][] somma(double a[][], double b[][]) {

        if (a.length != b.length && a[0].length != b[0].length) {
            return null;
        } else {
            int i, j;
            final int row = a.length, col = a[0].length;
            double r[][] = new double[row][col];

            for (i = 0; i < row; i++) {
                for (j = 0; j < col; j++) {
                    r[i][j] = a[i][j] + b[i][j];
                }
            }

            return r;
        }
    }
}
