package algebra;


/**
 *
 * @author bonny
 */
public class Trasposta {

   
    /**
     * 
     * @param M
     * @return
     */
    public static double[][] trasposta(double M[][]) {
        if (M != null) {
            int i, j;
            final int row = M.length, col = M[0].length;
            double tmp[][] = new double[col][row];

            for (i = 0; i < row; i++) {
                for (j = 0; j < col; j++) {
                    tmp[j][i] = M[i][j];
                }
            }
            return tmp;
        } else {
            return null;
        }
    }
}
