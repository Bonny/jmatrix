package algebra;

/**
 *
 * @author bonny
 */
public class Prodotto {

    /**
     * 
     * @param a
     * @param b
     * @return
     */
    public static double[][] prodotto(double a[][], double b[][]) {

        if (a[0].length != b.length) {
            return null;
        } else {
            int i, j, k;
            final int row = a.length, col = b[0].length;
            double c[][] = new double[row][col];

            for (i = 0; i < row; i++) {
                for (j = 0; j < col; j++) {
                    for (k = 0; k < col - 1; k++) {
                        c[i][j] += a[i][k] * b[k][j];
                    }

                }
            }
            return c;
        }
    }

    /**
     * 
     * @param x
     * @param M
     * @return
     */
    public static double[][] prodotto(double x, double M[][]) {

        double R[][] = new double[M.length][M[0].length];

        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[0].length; j++) {
                R[i][j] = x * M[i][j];
            }
        }
        return R;
    }
}
