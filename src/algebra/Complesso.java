/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algebra;

/**
 * 
 * @author bonny
 */
public class Complesso {

    private double Re, Im;

    /**
     * 
     */
    public Complesso() {
    }

    /**
     * 
     * @param Re
     * @param Im
     */
    public Complesso(double Re, double Im) {
        this.Re = Re;
        this.Im = Im;
    }

    /**
     * 
     * @param Re
     */
    public void setRe(double Re) {
        this.Re = Re;
    }

    /**
     * 
     * @param Im
     */
    public void setIm(double Im) {
        this.Im = Im;
    }

    /**
     * 
     * @return
     */
    public double getRe() {
        return this.Re;
    }

    /**
     * 
     * @return
     */
    public double getIm() {
        return this.Im;
    }
}
